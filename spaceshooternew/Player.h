#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include "Bullet.h"
#include <SFML/Audio.hpp>

class Player
{
public:
	// Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer);
	void Input();
	void Update(sf::Time frameTime);
	void Draw(sf::RenderWindow& gameWindow);
	void Reset();

	//Getter
	bool GetAlive();
	sf::FloatRect GetHitBox();

	// Setters
	void SetAlive(bool newAlive);

private:

	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenBounds;
	std::vector<Bullet>& bullets;
	sf::Texture& bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;
	bool alive;
};

