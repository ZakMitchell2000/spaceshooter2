#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include "Bullet.h"
#include <SFML/Audio.hpp>

class Enemy
{
public:
	// Constructor
	Enemy(sf::Texture& enemyTexture,
		sf::Vector2u newScreenBounds,
		std::vector<Bullet>& newBullets,
		sf::Texture& newBulletTexture,
		sf::SoundBuffer& firingSoundBuffer,
		std::vector< sf::Vector2f > newMovementPattern,
		sf::Vector2f startPosition);
	
	void Update(sf::Time frameTime);
	void Draw(sf::RenderWindow& gameWindow);

	// Getters
	bool GetAlive();
	sf::FloatRect GetHitBox();

	// Setters
	void SetAlive(bool newAlive);

private:

	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenBounds;
	std::vector<Bullet>* bullets;
	sf::Texture* bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;

	std::vector< sf::Vector2f > movementPattern;
	int currentInstruction;
	bool alive;
};

