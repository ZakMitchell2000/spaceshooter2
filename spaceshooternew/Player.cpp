#include "Player.h"
Player::Player(sf::Texture& playerTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer)
	: sprite(playerTexture)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, screenBounds(newScreenBounds)
	, bullets(newBullets)
	, bulletTexture(newBulletTexture)
	, bulletCooldownRemaining(sf::seconds(0.0f))
	, bulletCooldownMax(sf::seconds(0.5f))
	, bulletFireSound(firingSoundBuffer)
	, alive(true)
{
	// Avoid duplicating code by calling reset here instead of positioning the player manually
		Reset();
}
void Player::Input()
{
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}
	
	// If the player is pressing space, spawn a bullet
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)&&bulletCooldownRemaining <= sf::seconds(0.0f))
	{
		sf::Vector2f bulletPosition = sprite.getPosition();

		// Play firing sound
		bulletFireSound.play();

		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture.getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture.getSize().x / 2;
		bullets.push_back(Bullet(bulletTexture, screenBounds,bulletPosition, sf::Vector2f(1000, 0)));
		// reset bullet cooldown
		bulletCooldownRemaining = bulletCooldownMax;
	}
}
void Player::Update(sf::Time frameTime)
{
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	// LEFT
	if (newPosition.x < 0)
		newPosition.x = 0;
	// RIGHT
	if (newPosition.x + sprite.getTexture()->getSize().x > screenBounds.x)
		newPosition.x = screenBounds.x - sprite.getTexture()->getSize().x;
	// TOP
	if (newPosition.y < 0)
		newPosition.y = 0;
	// BOTTOM
	if (newPosition.y + sprite.getTexture()->getSize().y > screenBounds.y)
		newPosition.y = screenBounds.y - sprite.getTexture()->getSize().y;


	// Move the player
	sprite.setPosition(newPosition);

	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;
}
void Player::Draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(sprite);
}
void Player::Reset()
{
	sprite.setPosition(
		(screenBounds.x / 2 - sprite.getTexture()->getSize().x) * 0.25f,
		screenBounds.y / 2 - sprite.getTexture()->getSize().y / 2
	);
}
bool Player::GetAlive()
{
	return alive;
}
sf::FloatRect Player::GetHitBox()
{
	return sprite.getGlobalBounds();
}
void Player::SetAlive(bool newAlive)
{
	alive = newAlive;
}