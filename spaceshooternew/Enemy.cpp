#include "Enemy.h"
Enemy::Enemy(sf::Texture& enemyTexture,
	sf::Vector2u newScreenBounds,
	std::vector<Bullet>& newBullets,
	sf::Texture& newBulletTexture,
	sf::SoundBuffer& firingSoundBuffer,
	std::vector< sf::Vector2f > newMovementPattern,
	sf::Vector2f startPosition)
	: sprite(enemyTexture)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, screenBounds(newScreenBounds)
	, bullets(&newBullets)
	, bulletTexture(&newBulletTexture)
	, bulletCooldownRemaining(sf::seconds(0.0f))
	, bulletCooldownMax(sf::seconds(0.5f))
	, bulletFireSound(firingSoundBuffer)
    , movementPattern(newMovementPattern)
	, currentInstruction(0)
	, alive(true)
{
	sprite.setPosition(startPosition);
}
void Enemy::Update(sf::Time frameTime)
{
	if (currentInstruction >= movementPattern.size())
	{
		alive = false;

		return;
	}

	// Get target from instruction
	sf::Vector2f targetPoint = movementPattern[currentInstruction];

	// Get distance vector from target point
	sf::Vector2f distanceVector = targetPoint - sprite.getPosition();

	// Calculate direction vector 
	// Unit vecor of distance
	// By dividing distance vector by its magnitude (length)
	float distanceMag = std::sqrt(distanceVector.x * distanceVector.x + distanceVector.y * distanceVector.y);

	sf::Vector2f directionVector = distanceVector / distanceMag;

	float distanceToTravel = speed * frameTime.asSeconds();

	// Calculate new position
	// Old position + direction * distance traveled in that direction
	// (Just scaling our direction by how far we traveled in that direction
	sf::Vector2f newPosition = sprite.getPosition() + directionVector * distanceToTravel;

	// check if we will reach or over shoot our targer
	// we will reach or over shoot if the distance to out target
	// is less than the distance we will actually travel
	if (distanceMag <= distanceToTravel)
	{
		// we have reached the target
		newPosition = targetPoint;

		++currentInstruction;
	}

	// Move the player
	sprite.setPosition(newPosition);

	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;

	if (bulletCooldownRemaining <= sf::seconds(0.0f))
	{
		sf::Vector2f bulletPosition = sprite.getPosition();

		// Play firing sound
		bulletFireSound.play();

		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture->getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture->getSize().x / 2;
		bullets->push_back(Bullet(*bulletTexture, screenBounds, bulletPosition, sf::Vector2f(-1000, 0)));

		// reset bullet cooldown
		bulletCooldownRemaining = bulletCooldownMax;
	}
}
void Enemy::Draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(sprite);
}
bool Enemy::GetAlive()
{
	return alive;
}
sf::FloatRect Enemy::GetHitBox()
{
	return sprite.getGlobalBounds();
}
void Enemy::SetAlive(bool newAlive)
{
	alive = newAlive;
}
