// Library Includes	
// Library needed for using sprites, textures, and fonts
#include <SFML/Graphics.hpp>
// Library needed for playing music and sound effects
#include <SFML/Audio.hpp>
// Library for manipulating strings of text
#include <string>
// Library for handling collections of objects
#include <vector>
#include "Player.h"
#include "Star.h"
#include "Bullet.h"
#include "Enemy.h"
#include "SpawnData.h"

int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Space Shooter", sf::Style::Titlebar | sf::Style::Close);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------

	// Game Clock
	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;

	// Music
	sf::Music gameMusic;
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.setVolume(50);
	gameMusic.play();

	//Lose and win music
	sf::SoundBuffer lossBuffer;
	lossBuffer.loadFromFile("Assets/Audio/loss.ogg");
	sf::Sound lossSFX(lossBuffer);
	lossSFX.setVolume(50);


	// Bullets
	std::vector<Bullet> playerBullets;
	std::vector<Bullet> enemyBullets;
	sf::Texture playerBulletTexture;
	playerBulletTexture.loadFromFile("Assets/Graphics/playerBullet.png");
	sf::Texture enemyBulletTexture;
	enemyBulletTexture.loadFromFile("Assets/Graphics/enemyBullet.png");

	// Player
	sf::Texture playerTexture;
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	sf::SoundBuffer bulletFire;
	bulletFire.loadFromFile("Assets/Audio/fire.ogg");
	Player playerObject(playerTexture, gameWindow.getSize(), playerBullets, playerBulletTexture, bulletFire);

	// movement pattern
	std::vector< sf::Vector2f > zigZagPattern;
	zigZagPattern.push_back(sf::Vector2f(900, 900));
	zigZagPattern.push_back(sf::Vector2f(600, 300));
	zigZagPattern.push_back(sf::Vector2f(300, 900));

	std::vector< sf::Vector2f > zigZagPattern2;
	zigZagPattern2.push_back(sf::Vector2f((gameWindow.getSize().x * 0.75), 900));
	zigZagPattern2.push_back(sf::Vector2f((gameWindow.getSize().x * 0.5), 100));
	zigZagPattern2.push_back(sf::Vector2f(gameWindow.getSize().x * 0.25, 900));
	zigZagPattern2.push_back(sf::Vector2f(0, 100));
	// Enemy
	std::vector<Enemy> enemies;

	sf::Texture enemyTexture;
	enemyTexture.loadFromFile("Assets/Graphics/enemy.png");

	//Spawn Info
	std::vector<SpawnData> spawnData;
	int spawnIndex = 0;

	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,100),
		zigZagPattern2,
		sf::seconds(1.0f)});
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,100),
		zigZagPattern2,
		sf::seconds(1.0f) });
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,100),
		zigZagPattern2,
		sf::seconds(1.0f) });
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,100),
		zigZagPattern2,
		sf::seconds(1.0f) });
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,100),
		zigZagPattern2,
		sf::seconds(1.0f) });
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,100),
		zigZagPattern2,
		sf::seconds(1.0f) });
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,100),
		zigZagPattern2,
		sf::seconds(1.0f) });
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,100),
		zigZagPattern2,
		sf::seconds(1.0f) });

	sf::Time timeToSpawn = spawnData[0].delay;

	// Stars
	sf::Texture starTexture;
	starTexture.loadFromFile("Assets/Graphics/star.png");
	std::vector<Star> stars;
	int numStars = 5;
	for (int i = 0; i < numStars; ++i)
	{
		stars.push_back(Star(starTexture, gameWindow.getSize()));
	}

	// Score 
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Font/mainFont.ttf");

	int score = 0;
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: " + std::to_string(score));
	scoreText.setCharacterSize(50);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(50, 50);

	// Gameover
	bool gameOver = false;

	sf::Text gameOverText;
	gameOverText.setFont(gameFont);
	gameOverText.setString("GAME OVER\n\nPress R to restart\nor Q to quit");
	gameOverText.setCharacterSize(50);
	gameOverText.setFillColor(sf::Color::White);
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	// Win
	bool winState = false;

	sf::Text winText;
	winText.setFont(gameFont);
	winText.setString("You Win\n\nPress R to restart\nor Q to quit");
	winText.setCharacterSize(50);
	winText.setFillColor(sf::Color::White);
	winText.setStyle(sf::Text::Bold | sf::Text::Italic);
	winText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	// Game Loop
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;
		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed

			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		} // End event polling loop
		playerObject.Input();

		//if gameover
		//check for gameover options
		if (gameOver || winState)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				// End the game
				gameWindow.close();
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			{
				// Reset the game
				gameOver = false;
				winState = false;
				playerObject.Reset();
				score = 0;
				gameMusic.play();
				enemies.clear();
				enemyBullets.clear();
				playerBullets.clear();
				spawnIndex = 0;
			}
		}

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		// Get the time passed since the last frame and restart our game clock
		
		sf::Time frameTime = gameClock.restart();

		if (!gameOver && !winState)
		{
			if (playerObject.GetAlive())
			{
				playerObject.Update(frameTime);
			}

			scoreText.setString("Score: " + std::to_string(score));

			// Update the stars
			for (int i = 0; i < stars.size(); ++i)
			{
				stars[i].Update(frameTime);
			}
			// Update the player bullets
			for (int i = playerBullets.size() - 1; i >= 0; --i)
			{
				playerBullets[i].Update(frameTime);
				// If the bullet is dead, delete it
				if (!playerBullets[i].GetAlive())
				{
					// Remove the item from the vector
					playerBullets.erase(playerBullets.begin() + i);
				}
			}

			// Update the enemy bullets
			for (int i = enemyBullets.size() - 1; i >= 0; --i)
			{
				enemyBullets[i].Update(frameTime);
				// If the bullet is dead, delete it
				if (!enemyBullets[i].GetAlive())
				{
					// Remove the item from the vector
					enemyBullets.erase(enemyBullets.begin() + i);
				}
			}
			// Update the enemy
			for (int i = enemies.size() - 1; i >= 0; --i)
			{
				enemies[i].Update(frameTime);

				// If the enemies is dead, delete it
				if (!enemies[i].GetAlive())
				{
					// Remove the item from the vector
					enemies.erase(enemies.begin() + i);
				}
			}


			timeToSpawn -= frameTime;
			// Check if it is time to spawn an enemy
			if (timeToSpawn <= sf::seconds(0) && spawnIndex < spawnData.size())
			{
				//spawn enemy
				enemies.push_back(Enemy(enemyTexture,
					gameWindow.getSize(),
					enemyBullets,
					enemyBulletTexture,
					bulletFire,
					spawnData[spawnIndex].pattern,
					spawnData[spawnIndex].position));

				++spawnIndex;
				if (spawnIndex < spawnData.size())
					timeToSpawn = spawnData[spawnIndex].delay;
			}

			// Check for collisions between the players bullets and the enemies
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				for (int j = 0; j < enemies.size(); ++j)
				{
					sf::FloatRect bulletBounds = playerBullets[i].GetHitBox();
					sf::FloatRect enemyBounds = enemies[j].GetHitBox();

					if (bulletBounds.intersects(enemyBounds))
					{
						//Kill the bullet and the enemy
						playerBullets[i].SetAlive(false);
						enemies[j].SetAlive(false);
						score = score + 100;
					}
				}
			}

			for (int i = 0; i < enemyBullets.size(); ++i)
			{
				sf::FloatRect enemyBulletBounds = enemyBullets[i].GetHitBox();
				sf::FloatRect playerBounds = playerObject.GetHitBox();

				if (enemyBulletBounds.intersects(playerBounds))
				{
					//Kill the bullet and the enemy
					//enemyBullets[i].SetAlive(false);
					//playerObject.SetAlive(false);
					gameOver = true;
					gameMusic.stop();
					lossSFX.play();
				}
			}

			// Check if we won
			if (spawnIndex >= spawnData.size() && enemies.empty())
			{
				winState = true;
				gameMusic.stop();

			}
		}
		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------
		// Clear the window to a single colour
		gameWindow.clear(sf::Color(15, 15, 15));

		
		// Draw the stars
		for (int i = 0; i < stars.size(); ++i)
		{
			stars[i].DrawTo(gameWindow);
		}

		if (!gameOver && !winState)
		{
			// Draw the player bullets
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				playerBullets[i].DrawTo(gameWindow);
			}

			// Draw the enemy bullets
			for (int i = 0; i < enemyBullets.size(); ++i)
			{
				enemyBullets[i].DrawTo(gameWindow);
			}
			// Draw game objects
			playerObject.Draw(gameWindow);
			
			if (gameOver)
			{
				gameWindow.draw(gameOverText);
			}

			for (int i = 0; i < enemies.size(); ++i)
			{
				enemies[i].Draw(gameWindow);
			}
		}
		if (gameOver)
		{
			gameWindow.draw(gameOverText);
		}
		if (winState)
		{
			gameWindow.draw(winText);
		}
		// Draw Score
		gameWindow.draw(scoreText);
		
		// Display the window contents on the screen
		gameWindow.display();


	} // End of Game Loop
	return 0;
}